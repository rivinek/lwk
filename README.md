# Do I know you?
Algorithm for face recognition + Haar Classifier

## Requirements:
 * [Python 2.7](https://www.python.org/download/releases/2.7/)
 * [virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/)
 * [OpenCV](http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_tutorials.html)

## Installation
```
$ mkvirtualenv doiknowyou
```
To use this virtualenv:
```
$ workon doiknowyou
$ cdvirtualenv doiknowyou
```
To install Python module:
```
$ pip install -e .
```