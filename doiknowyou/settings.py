import os

PROJECT_DIR = os.path.dirname(__file__)

IMAGES_DIR = os.path.join(PROJECT_DIR, 'images')
CASCADE_PATH = os.path.join(PROJECT_DIR, 'haarcascade_frontalface_default.xml')
