import os
import cv
import cv2
import numpy as np
import random
from PIL import Image
from StringIO import StringIO

from doiknowyou.settings import IMAGES_DIR


CASCADE = "./haarcascade_frontalface_default.xml"
face_dir = "./train/"

min_size = (20, 20)
IMAGE_SCALE = 2
haar_scale = 1.2
min_neighbors = 3
haar_flags = 0
label_dict = {}
variable_faces = []


def get_all_images(name):
    counter_id = 0
    path_to_user_images = os.path.join(IMAGES_DIR, name, 'postprocessed')
    face_list = os.listdir(path_to_user_images)
    for face_name in face_list:
        try:
            full_path = os.path.join(path_to_user_images, face_name)
            face = cv.LoadImage(full_path, cv2.IMREAD_GRAYSCALE)
        except IOError:
            continue
        counter_id += 1
        yield face[:, :], counter_id


def train_recognizers(name, recognizers):
    images = []
    labels = []

    for recognizer in recognizers:
        for face, id_counter in get_all_images(name):
            images.append(np.asarray(face))
            labels.append(id_counter)

        image_array = np.asarray(images)
        label_array = np.asarray(labels)
        recognizer.train(image_array, label_array)
    return recognizers


def train(name):
    num_recognizers = 3
    recognizers = []
    for j in xrange(num_recognizers):
        lbh_recognizer = cv2.createLBPHFaceRecognizer()
        recognizers.append(lbh_recognizer)
    return train_recognizers(name, recognizers)


def check_test(target_name):
    recognizers = train(target_name)
    average_confidence = 0
    TEST_DIR = os.path.join(IMAGES_DIR, target_name, 'tests')
    tests_names = os.listdir(TEST_DIR)
    total_trainset = len(os.listdir(os.path.join(IMAGES_DIR,
                                                 target_name,
                                                 'postprocessed')))
    succ, miss = 0, 0

    for test_name in tests_names:
        test_image = TEST_DIR + '/' + test_name
        tester_image = cv.LoadImage(test_image, cv2.IMREAD_GRAYSCALE)

        for lbh_recognizer in recognizers:
            face_img = np.asarray(tester_image[:, :])
            label, confidence = lbh_recognizer.predict(face_img)
            average_confidence += confidence

        average_confidence /= len(recognizers)

        if average_confidence < 30.00:
            succ += 1
        else:
            miss += 1

    return ('Total train images: {}\n'
            'Total test images: {}\n'
            'Success recognized: {}\n'
            'Miss recognized: {}\n'
            'Succes rate: {} %').format(total_trainset,
                                        len(tests_names),
                                        succ,
                                        miss,
                                        (succ/float(len(tests_names)))*100)
