import os
import shutil

import cv2

from doiknowyou.settings import CASCADE_PATH
from doiknowyou.settings import IMAGES_DIR
from doiknowyou.utils import create_folder


def detect_and_cut_face_from_image(image_name, input_path, output_path):
    face_cascade = cv2.CascadeClassifier(CASCADE_PATH)

    img = cv2.imread(os.path.join(input_path, image_name))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(
        img,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )
    for (x, y, w, h) in faces:
        rec_x = x + w
        rec_y = y + h
        cv2.rectangle(img, (x, y), (rec_x, rec_y), (0, 255, 0), 2)
        if x != rec_x or y != rec_y:
            cv2.imwrite(os.path.join(output_path, image_name),
                        img[y:rec_y, x:rec_x])


def preprocess_all_images(name):
    path_to_user_images = os.path.join(IMAGES_DIR, name)
    input_path = path_to_user_images + '/preprocessed/'
    output_path = path_to_user_images + '/postprocessed/'
    create_folder(output_path)
    input_image_list = os.listdir(input_path)

    for image_name in input_image_list:
        detect_and_cut_face_from_image(image_name, input_path, output_path)


def create_test_sets(name):
    path_to_user_images = os.path.join(IMAGES_DIR, name)
    postprocessed_path = path_to_user_images + '/postprocessed/'
    listdir = os.listdir(postprocessed_path)
    only_first_photo = [photo for photo in listdir
                        if photo.endswith('1.jpg')]

    for photo in only_first_photo:
        shutil.move(postprocessed_path + '/' + photo,
                    path_to_user_images + '/tests/' + photo)
        print 'Moved ' + photo